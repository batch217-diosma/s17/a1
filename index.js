/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function printWelcomeMessage() {
	let fullName = prompt('What is your name?');
	console.log('Hello, ' + fullName);
	let age = prompt('How old are you?');
	console.log('You are ' + age + ' years old.');
	let location = prompt('Where do you live?');
	console.log('You live in ' + location);
	alert('Thank you for your input!');

}
printWelcomeMessage();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function printBandName() {
	let band1 = 'Kamikazee';
	console.log('1. ' + band1);
	let band2 = 'Parokya ni Edgar';
	console.log('2. ' + band2);
	let band3 = 'Eraserheads';
	console.log('3. ' + band3);
	let band4 = 'Paramore';
	console.log('4. ' + band4);
	let band5 = 'Silent Sanctuary';
	console.log('5. ' + band5);
}
printBandName();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

		
	function printMovieName() {
	let movie1 = 'The Lord of the Rings: The Return of the King';
	console.log('1. ' + movie1);
	console.log('Rotten Tomatoes Rating: 93%');
	let movie2 = 'The Lord of the Rings: The Two Towers';
	console.log('2. ' + movie2);
	console.log('Rotten Tomatoes Rating: 95%');
	let movie3 = 'The Lord of the Rings: The Fellowship of the Ring';
	console.log('3. ' + movie3);
	console.log('Rotten Tomatoes Rating: 91%');
	let movie4 = 'Casino Royale';
	console.log('4. ' + movie4);
	console.log('Rotten Tomatoes Rating: 94%');
	let movie5 = 'Ready Player One';
	console.log('5. ' + movie5);
	console.log('Rotten Tomatoes Rating: 72%');

}
printMovieName();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

//printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
	alert('Thank you for your input!');
}
printFriends();

//console.log(friend1);
//console.log(friend2);